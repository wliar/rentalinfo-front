# Assignment 2 - Agile Software Practice.

Name: Rental Information System

## Client UI.

Screenshots

![][home]

>>Home Page

![][hi]

>>Allows the user view, filter, edit and delete the house information

![][c]

>>Allows the user create own house information

![][e]

>>Allows the user update own house information

![][ol]

>>Allows the user view the owner information

![][p]

>>Allows the user view and update own profile

![][si]

>>Sign In

![][su]

>>Sign Up

## E2E/Cypress testing.
Cypress Dashboard with Screenshots
https://dashboard.cypress.io/projects/daetns/runs


[home]: ./img/home.PNG
[hi]: ./img/hi.PNG
[c]: ./img/c.PNG
[e]: ./img/e.PNG
[ol]: ./img/ol.PNG
[p]: ./img/p.PNG
[si]: ./img/si.PNG
[su]: ./img/su.PNG
