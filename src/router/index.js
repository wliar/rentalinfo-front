import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import Renting from '@/components/pages/Renting.vue'
import HouseInfo from '@/components/pages/HouseInfo.vue'
import Owners from '@/components/pages/Owners.vue'
import EditHouseInfo from '@/components/pages/EditHouseInfo.vue'
import CreateHouseInfo from '@/components/pages/CreateHouseInfo.vue'
import MyInfo from '@/components/pages/MyInfo.vue'
import Signin from '@/components/pages/Login.vue'
import Signup from '@/components/pages/Register.vue'
Vue.use(Router)

var router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/houses',
      name: 'houses',
      component: Renting
    },
    {
      path: '/renting',
      name: 'renting',
      component: Renting
    },
    {
      path: '/houseinfo',
      name: 'houseinfo',
      component: HouseInfo
    }, {
      path: '/createHouseInfo',
      name: 'createHouseInfo',
      component: CreateHouseInfo
    }, {
      path: '/edithouseinfo',
      name: 'edithouseinfo',
      component: EditHouseInfo
    }, {
      path: '/myinfo',
      name: 'myinfo',
      component: MyInfo
    },
    {
      path: '/owners',
      name: 'owners',
      component: Owners
    },
    {
      path: '/signin',
      name: 'signin',
      component: Signin
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    }
  ]
})

router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('authUser'))
    if (authUser && authUser.access_token) {
      next()
    } else {
      next({ name: 'home' })
    }
  }
  next()
})

export default router
