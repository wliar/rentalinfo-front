// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import * as VueGoogleMaps from 'vue2-google-maps'
import axios from './utils/vue-axios'
import Paginate from 'vuejs-paginate'
Vue.config.productionTip = false

Vue.component('paginate', Paginate)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBs6V8pF9yP4fraUqESZNAaU4_4SxgA1YI',
    libraries: 'places' // This is required if you use the Autocomplete plugin
  },
  installComponents: true
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  components: { App },
  template: '<App/>'
})
