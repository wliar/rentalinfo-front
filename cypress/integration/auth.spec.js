const apiURL = 'http://localhost:3000'
describe('Auth Test', function () {
  beforeEach(() => {
    cy.request('POST', apiURL + '/users/signin', { username: '123', password: '123' }).then((data) => {
      // console.log(data)
      localStorage.auth = true
      localStorage.user = JSON.stringify({ data: data.body.user })
    })
  })

  it('test authentication', function () {
    cy.visit('http://localhost:8080')
    cy.get('.nav-link').contains('Create')
    cy.get('.nav-link').contains('Profile')
  })

  it('test create a renting', function () {
    let address = 'old address'
    cy.visit('http://localhost:8080')
    cy.get('a[href="#/createHouseInfo"]').click()
    cy.get('input[id="rooms"]').type('3')
    cy.get('input[id="price"]').type('400')
    cy.get('input[id="address"]').type(address)
    cy.get('button[type="submit"]').click()
    cy.get('.alert').contains('Added Successfully!')
    cy.screenshot('creat houseinfo')
  })

  it('test Update the a renting', () => {
    let address = 'new address'
    cy.wait(1000)
    cy.get('a[href="#/houses"]').click()
    cy.get('.btn-outline-secondary').eq(0).click()
    cy.get('input[id="rooms"]').type('3')
    cy.get('input[id="price"]').type('400')
    cy.get('input[id="address"]').type(address)
    cy.get('button[type="submit"]').click()
    cy.get('.alert').contains('Modified Successfully!')
    cy.screenshot('update houseinfo')
  })

  it('test delete the new renting', () => {
    cy.wait(1000)
    cy.get('a[href="#/houses"]').click()
    cy.get('table').find('.btn-outline-danger').should('have.length', 1)
    cy.wait(1000)
    cy.get('.btn-outline-danger').click()
    cy.get('table').find('.btn-outline-danger').should('have.length', 0)
    cy.screenshot('delete houseinfo')
  })

  it('test logout', () => {
    cy.get('a[id="logout"]').click()
    cy.get('a[id="signin"]').should('have.length', 1)
    cy.get('a[id="signup"]').should('have.length', 1)
    cy.screenshot('logout')
  })
})
