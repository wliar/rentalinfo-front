
const apiURL = 'http://localhost:8080'
describe('Filters Test', function () {
  it('address filter', function () {
    cy.visit(apiURL)
    cy.get('a[href="#/houses"]').click()
    cy.wait(3000)
    cy.get('input[id=address]').type('river')
    cy.get('table').find('tr').should('have.length', 3)
    cy.get('input[id=address]').clear()
    cy.get('table').find('tr').its('length').should('be.gte', 4)
    cy.screenshot('search address')
  })

  it('room filter', function () {
    cy.visit(apiURL)
    cy.get('a[href="#/houses"]').click()
    cy.get('input[id=room]').type(123)
    cy.get('table').find('tr').should('have.length', 2)
    cy.screenshot('search room number')
  })

  it('owner filter', function () {
    cy.visit(apiURL)
    cy.get('a[href="#/houses"]').click()
    cy.get('input[id=owner]').type(123)
    cy.get('table').find('tr').its('length').should('be.gte', 4)
  })

  it('price filter', function () {
    cy.visit(apiURL)
    cy.get('a[href="#/houses"]').click()
    cy.get('input[id=price]').type(300)
    cy.get('table').find('tr').its('length').should('be.gte', 4)
    cy.screenshot('search price')
  })
})

describe('NavBar Test', function () {
  it('home button', function () {
    cy.get('a[href="#/"]').click()
    cy.get('h1').contains('Rental Information System')
    cy.screenshot('Navbar')
  })

  it('owners button', function () {
    cy.get('a[href="#/owners"]').click()
    cy.get('div[class="card"]').its('length').should('be.gt', 0)
    cy.screenshot('view house owner')
  })
})
